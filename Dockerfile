FROM ubuntu:20.04

RUN apt-get update \
  && rm -rf /var/lib/apt/lists/*

CMD ["-h"]
